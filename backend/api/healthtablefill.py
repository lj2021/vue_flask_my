from flask import Blueprint, current_app, request
from flask import jsonify
from flask_bcrypt import generate_password_hash, check_password_hash
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
import uuid
import sqlite3
from . import api_rest
import datetime

class Healthtablefill(Resource):
    @jwt_required
    def get(self): #仅返回healthtable信息
        # 根据用户名查adminid
        jwt_identity = get_jwt_identity()
        username = jwt_identity['username']
        # print('username=', username)
        conn = sqlite3.connect('backend/outbreak-management-system.db')
        c = conn.cursor()
        order = "select ID from ADMINISTRATOR where USERNAME='" + username +"'"
        # print(order)
        c.execute(order)
        cursor = c.fetchall()
        adminid = cursor[0][0]
        # print(adminid)

        # 查询pid和adminid
        todaytime = datetime.date.today()
        order = "select * from HEALTHTABLE where (FILLINGTIME='" + str(todaytime) +"' and ADMINID='" + adminid + "')"
        print(order)
        c.execute(order)
        cursor = c.fetchall()
        print(cursor)
        result = jsonify({'healthtabel':cursor})
        print("len = ",range(len(cursor)))
        namelist = []
        for i in range(len(cursor)):
            pid = cursor[i][7]
            order = "select NAME from PROPRIETOR where ID='" + pid + "'"
            c.execute(order)
            print(order)
            cursor2 = c.fetchall() 
            name = cursor2[0][0]
            print(name)
            namelist.append(name)

        return jsonify({'healthtable':cursor, 'name':namelist})

    @jwt_required
    def post(self):
        body = request.get_json()
        formno = uuid.uuid1()
        infectstatus = body['infectstatus']
        temprature = body['temprature']
        fillingtime = datetime.date.today()
        livingstatus = body['livingstatus']
        medicalcondition = body['medicalcondition']

        print("m = ",medicalcondition)
        jwt_identity = get_jwt_identity()
        print(jwt_identity)
        if (jwt_identity['authority'] == 0): # 管理员不能进行iotable登记
            return {'authority':0}

        username = jwt_identity['username']
        
        conn = sqlite3.connect('backend/outbreak-management-system.db')
        c = conn.cursor()

        # 查询pid和adminid
        order = "select ID, ADMINID from PROPRIETOR where USERNAME='" + username +"'"
        print(order)
        c.execute(order)
        cursor = c.fetchall()
        database_pid = cursor[0][0]
        database_adminid = cursor[0][1]

        # 写入healthtable
        if (medicalcondition is None):
            medicalcondition = "未就医"
            order = "insert into HEALTHTABLE values ('"+ str(formno) + "'," + str(infectstatus) + ",'" + str(medicalcondition) + "'," + str(temprature) + ",'" + str(fillingtime) + "'," + str(livingstatus) + ",'" + database_adminid + "','" + database_pid + "')"
        else:
            order = "insert into HEALTHTABLE values ('"+ str(formno) + "'," + str(infectstatus) + ",'" + str(medicalcondition) + "'," + str(temprature) + ",'" + str(fillingtime) + "'," + str(livingstatus) + ",'" + database_adminid + "','" + database_pid + "')"
        print(order)
        c.execute(order)
        conn.commit()
        conn.close()
        return {"submit":"ok"}
        
api_rest.add_resource(Healthtablefill, '/healthtable')