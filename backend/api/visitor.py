from flask import Blueprint, current_app, request
from flask_bcrypt import generate_password_hash, check_password_hash
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
import uuid
import sqlite3
from . import api_rest

class Visitor(Resource):
    @jwt_required
    def post(self):
        body = request.get_json()
        id = body['id']
        name = body['name']
        age = body['age']
        sex = body['sex']
        telephone = body['telephone']
        temperature = body['temperature']
        healthecode = body['healthcode']

        jwt_identity = get_jwt_identity()
        print(jwt_identity)
        if (jwt_identity['authority'] != 0): # 非管理员不能进行iotable登记
            return {'authority':0}, 403
        
        order = "insert into VISITOR values ('"+ id + "','" + name + "'," + age + "," + sex + "," + temperature + "," + healthecode + "','" + telephone +"')"
        print(order)
        conn = sqlite3.connect('backend/outbreak-management-system.db')
        c = conn.cursor()
        c.execute(order)
        conn.commit()
        conn.close()
        return {'name':name}, 200

api_rest.add_resource(Visitor, '/visitor')