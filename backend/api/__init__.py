from flask import Blueprint, current_app
from flask_restful import Api

api_bp = Blueprint('api_bp', __name__, url_prefix='/api')
api_rest = Api(api_bp)

from .testapi import *
from .register import *
from .login import *
from .iotablefill import *
from .healthtablefill import *
from .personinfo import *