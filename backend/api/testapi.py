from flask import Blueprint, current_app, request
from flask_restful import Resource
from . import api_rest

class HelloWorld(Resource):
    def get(self):
        return {"hello":"world"}

    def post(self):
        # print("ok")
        print(request.json)

api_rest.add_resource(HelloWorld, '/testapi')