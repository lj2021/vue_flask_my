from flask import Blueprint, current_app, request
from flask import jsonify
from flask_bcrypt import generate_password_hash, check_password_hash
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
import uuid
import sqlite3
from . import api_rest
import datetime


class Iotablefill(Resource):
    @jwt_required
    def get(self):
        jwt_identity = get_jwt_identity()
        # 获取用户id——adminid
        username = jwt_identity['username']
        conn = sqlite3.connect('backend/outbreak-management-system.db')
        c = conn.cursor()
        order = "select ID from ADMINISTRATOR where USERNAME='" + username +"'"
        # print(order)
        c.execute(order)
        cursor = c.fetchall()
        adminid = cursor[0][0]

        todaytime = datetime.date.today()
        order = "select * from IOTABLE where (FILLINGTIME='" + str(todaytime) +"' and ADMINID='" + adminid + "')"
        print(order)
        c.execute(order)
        cursor = c.fetchall()
        print(cursor)

        namelist = []
        for i in range(len(cursor)):
            formno = cursor[i][0]
            order = "select NAME from VISITOR where ID in (select PID from VISITORIOFILL where FORMNO='" + formno + "')"
            c.execute(order)
            print(order)
            cursor2 = c.fetchall()
            print(cursor2)
            if(len(cursor2) == 0):
                order = "select NAME from PROPRIETOR where ID in (select PID from PROPRIETORIOFILL where FORMNO='" + formno + "')"
                c.execute(order)
                print(order)
                cursor2 = c.fetchall()
            print(cursor2)
            name = cursor2[0][0]
            print(name)
            namelist.append(name)
        return jsonify({'iotable':cursor, 'name':namelist})

    @jwt_required
    def post(self):
        body = request.get_json()
        formno = uuid.uuid1()
        identity = body['identity']
        roomnumber = body['roomnumber']
        corridornumber = body['corridornumber']
        fillingtime = datetime.date.today()
        iostatus = body['iostatus']
        ioreason = body['ioreason']
        temperature = body['temperature']
        healthcode = body['healthcode']
        pid = body['id']
        name = body['name']
        age = body['age']
        sex = body['sex']
        telephone = body['telephone']

        # 根据用户名查admin
        jwt_identity = get_jwt_identity()
        username = jwt_identity['username']
        print(username)
        # print('username=', username)
        conn = sqlite3.connect('backend/outbreak-management-system.db')
        c = conn.cursor()
        order = "select ID from ADMINISTRATOR where USERNAME='" + username +"'"
        # print(order)
        c.execute(order)
        cursor = c.fetchall()
        adminid = cursor[0][0]

        jwt_identity = get_jwt_identity()
        print(jwt_identity)
        if (jwt_identity['authority'] != 0): # 非管理员不能进行iotable登记
            return {'authority':1}, 403

        conn = sqlite3.connect('backend/outbreak-management-system.db')
        c = conn.cursor()
        # 写入iotable
        order = "insert into IOTABLE values ('"+ str(formno) + "'," + str(identity) + "," + str(temperature) + "," + str(healthcode) + ",'" + roomnumber + "','" + corridornumber + "','" + str(fillingtime) + "'," + str(iostatus) + ",'" + ioreason + "','" + adminid + "')"
        print(order)
        c.execute(order)

        # 写入VISITOR表
        if (identity == 0):
            order = "insert into VISITOR values ('" + pid + "','" + name + "'," + str(age) + "," + str(healthcode) + ",'" + telephone + "')"
            print(order)
            c.execute(order)
        # 写入iofill表
        # 需要根据identity来填表 0为住户 1为访客
        if (identity == 1):
            order = "insert into PROPRIETORIOFILL values ('"+ str(formno) + "','" + pid + "')"
            print(order)
        elif (identity == 0):
            order = "insert into VISITORIOFILL values ('"+ str(formno) + "','" + pid + "')"
            print(order)
        else:
            return {'identity':'not 0/1'}, 403
        c.execute(order)
        conn.commit()
        conn.close()
        return {"work":"ok"}
        
api_rest.add_resource(Iotablefill, '/iotable')