import os
from flask import Flask, current_app, send_file
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_cors import *


from .api import api_bp

# flask create
backend = Flask(__name__)

CORS(backend, supports_credentials=True)
backend.config.from_envvar('ENV_FILE_LOCATION')
bcrypt = Bcrypt(backend)
jwt = JWTManager(backend)

@jwt.user_identity_loader
def add_identity_to_access_token(identity):
    print(identity)
    return {
        'username': identity[0],
        'authority': identity[1]
    }

backend.register_blueprint(api_bp)