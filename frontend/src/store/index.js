import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: window.sessionStorage.getItem("user"),
    token: window.sessionStorage.getItem("token"),
    identity: window.sessionStorage.getItem("identity"),
  },
  mutations: {
    set_user: (state, new_user) => {
      state.user = new_user;
    },
    Signin: (state, response) => {
      state.token = response.token;
      state.user = response.username;
      state.identity = 1;
      window.sessionStorage.setItem("user", response.username);
      window.sessionStorage.setItem("token", response.token);
      window.sessionStorage.setItem("identity", 1);
    },
    Signout: (state) => {
      (state.token = ""),
      (state.user = ""),
      (state.identity = "")
      window.sessionStorage.removeItem("token");
      window.sessionStorage.removeItem("user");
      window.sessionStorage.removeItem("identity");
    },
    AdminSignin: (state, response) => {
      state.token = response.token;
      state.user = response.username;
      state.identity = 0;
      window.sessionStorage.setItem("user", response.username);
      window.sessionStorage.setItem("token", response.token);
      window.sessionStorage.setItem("identity", 0);
    },
  },
  actions: {},
  modules: {},
});
