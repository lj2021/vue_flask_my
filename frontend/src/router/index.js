import Vue from "vue";
import VueRouter from "vue-router";

import Home from "@/components/Home"; //主页面主体组件
import Signup from "@/components/Signup"; //注册
import Signin from "@/components/Signin"; //登录
import AdminSignup from "@/components/AdminSignup";
import AdminSignin from "@/components/AdminSignin";
// import IOTableSubmit from "@/components/IOTableSubmit";
// import PersonInfo from '@/components/PersonInfo' //个人信息组件

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    istoken:true,
    meta: { istoken: true },
    children: [
      //注册嵌套路由相关信息
      {
        path: "HomePage",
        meta: { istoken: true, title: "首页" },
        component: (resolve) => require(["@/components/HomePage"], resolve),
        name: "HomePage",
      },
      {
        path: "IOTableSubmit",
        meta: { istoken: true, isadmin: 0, title: "首页" },
        component: (resolve) =>
          require(["@/components/IOTableSubmit"], resolve),
        name: "IOTableSubmit",
      },
      {
        path: "HealthTableSubmit",
        meta: { istoken: true, isadmin: 1, title: "首页" },
        component: (resolve) =>
          require(["@/components/HealthTableSubmit"], resolve),
        name: "HealthTableSubmit",
      },
      {
        path: "IOTableQuery",
        meta: { istoken: true, isadmin: 0, title: "首页" },
        component: (resolve) => require(["@/components/IOTableQuery"], resolve),
        name: "IOTableQuery",
      },
      {
        path: "HealthTableQuery",
        meta: { istoken: true, isadmin: 0, title: "首页" },
        component: (resolve) =>
          require(["@/components/HealthTableQuery"], resolve),
        name: "HealthTableQuery",
      },
      {
        path: "PersonInfo",
        meta: { istoken: true, isadmin: 1, title: "首页" },
        component: (resolve) => require(["@/components/PersonInfo"], resolve),
        name: "PersonInfo",
      },
    ],
  },
  {
    path: "/signup",
    name: "signup",
    component: Signup,
  },
  {
    path: "/signin",
    name: "signin",
    component: Signin,
  },
  {
    path: "/adminsignup",
    name: "adminsignup",
    component: AdminSignup,
  },
  {
    path: "/adminsignin",
    name: "adminsignin",
    component: AdminSignin,
  },
];

const router = new VueRouter({
  routes,
});

export default router;


