import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import store from "./store";
import axios from "axios"; //引入前后端通讯组件
import vuex from "vuex"; //引入状态管理组件
import { alertBox } from "@/api/MessageBox.js"; //封装消息提醒组件

Vue.use(ElementUI);
Vue.use(vuex);

axios.defaults.baseURL = "http://127.0.0.1:5000";
axios.setTimeout = 2000;
Vue.prototype.alertBox = alertBox; //将消息提醒组件注册至vue对象链
Vue.prototype.$axios = axios; //将axios注册进vue对象链中
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

router.beforeEach((to, from, next) => {
  console.log(
    "isadmin = " + to.meta.isadmin + " identity = " + store.state.identity
  );
  if (to.meta.istoken) {
    //未定义该项或者该项为假，则此页面不需要登录权限
    if (store.state.token) {
      //如果当前状态store中有token项，说明此时用户已经登陆
      // document.title = to.meta.title;
      if (to.meta.isadmin == undefined) {
        next();
        console.log("1")
      }
      else if(to.meta.isadmin != store.state.identity){
        next({ path:"/HomePage" })
      }
      next();
    } else {
      // console.log("hello2");
      //console.log(to)
      next({ path: "/signin" });
    }
  } else {
    next();
  }
});
